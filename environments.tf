locals {
  repositories = [
    data.gitlab_project.zonage.id,
    data.gitlab_project.grist_form.id,
  ]
}

module "development" {
  source                   = "./environment"
  gitlab_environment_scope = "development"
  kubeconfig               = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  base-domain              = var.dev_base-domain
  grist_form_base_domain   = "formulaires.${var.dev_base-domain}"
  namespace                = "${var.project_slug}-development"
  repositories             = local.repositories
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 8
  namespace_quota_max_memory = "12Gi"

  zonage_project_id     = data.gitlab_project.zonage.id
  grist_form_project_id = data.gitlab_project.grist_form.id
  monitoring_org_id     = random_string.development_secret_org_id.result
}

module "production" {
  source                   = "./environment"
  gitlab_environment_scope = "production"
  kubeconfig               = data.scaleway_k8s_cluster.prod.kubeconfig[0]
  base-domain              = var.prod_base-domain
  namespace                = var.project_slug
  repositories             = local.repositories
  project_slug             = var.project_slug
  project_name             = var.project_name

  namespace_quota_max_cpu    = 8
  namespace_quota_max_memory = "12Gi"

  zonage_project_id     = data.gitlab_project.zonage.id
  grist_form_project_id = data.gitlab_project.grist_form.id
  monitoring_org_id     = random_string.production_secret_org_id.result

  grist_form_base_domain     = var.production_grist_form_base_domain
  grist_form_db_path         = var.production_grist_form_db_path
  grist_form_grist_api_token = var.production_grist_form_grist_api_token
  grist_form_grist_url       = "https://grist.incubateur.anct.gouv.fr/"
}
