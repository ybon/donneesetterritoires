variable "base-domain" {
  type = string
}
variable "grist_form_base_domain" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "gitlab_environment_scope" {
  type = string
}

variable "namespace" {
  type = string
}

variable "repositories" {
  type = list(number)
}

variable "namespace_quota_max_cpu" {
  type    = number
  default = 2
}

variable "namespace_quota_max_memory" {
  type    = string
  default = "12Gi"
}

variable "zonage_project_id" {
  type = number
}
variable "grist_form_project_id" {
  type = number
}

variable "monitoring_org_id" {
  type = string
}

variable "grist_form_grist_url" {
  type    = string
  default = ""
}
variable "grist_form_db_path" {
  type      = string
  default   = ""
  sensitive = true
}
variable "grist_form_grist_api_token" {
  type      = string
  default   = ""
  sensitive = true
}
