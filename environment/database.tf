locals {
  db_replicas = contains(["development", "production"], var.gitlab_environment_scope) ? 3 : 1
}
module "postgresql" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.22"

  chart_name = "postgresql"
  namespace  = module.namespace.namespace
  values = [
    file("${path.module}/database${var.gitlab_environment_scope != "production" ? "_new" : ""}.yaml"),
  ]
  pg_replicas    = local.db_replicas
  pg_volume_size = "5Gi"
}
