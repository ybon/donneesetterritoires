module "namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "2.0.0"
  namespace         = var.namespace
  max_cpu_requests  = var.namespace_quota_max_cpu
  max_memory_limits = var.namespace_quota_max_memory
  project_name      = var.project_name
  project_slug      = var.project_slug

  default_container_cpu_requests  = "20m"
  default_container_memory_limits = "16Mi"
}
