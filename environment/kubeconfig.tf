locals {
  environment-slug = var.gitlab_environment_scope == "*" ? "review" : var.gitlab_environment_scope
}

module "kubeconfig_vincentviers" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.2.0"

  filename               = "donnees-vincent-${local.environment-slug}.yml"
  namespace              = module.namespace.namespace
  username               = "vincent"
  project_slug           = var.project_slug
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
module "kubeconfig_vincentlara" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.2.0"

  filename               = "donnees-vincentlara-${local.environment-slug}.yml"
  namespace              = module.namespace.namespace
  username               = "vincentlara"
  project_slug           = var.project_slug
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
module "kubeconfig_ybon" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.2.0"

  filename               = "donnees-ybon-${local.environment-slug}.yml"
  namespace              = module.namespace.namespace
  username               = "ybon"
  project_slug           = var.project_slug
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
module "kubeconfig_ronanamicel" {
  source  = "gitlab.com/vigigloo/tf-modules/generatescopedkubeconfig"
  version = "0.2.0"

  filename               = "donnees-ronanamicel-${local.environment-slug}.yml"
  namespace              = module.namespace.namespace
  username               = "ronanamicel"
  project_slug           = var.project_slug
  project_name           = var.project_name
  role_name              = module.namespace.role_name
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = var.kubeconfig.cluster_ca_certificate
}
