module "configure_zonage_for_deployment" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version = "0.1.0"

  repositories = [var.repositories[0]]
  scope        = var.gitlab_environment_scope

  cluster_user           = module.namespace.user
  cluster_token          = module.namespace.token
  cluster_namespace      = module.namespace.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)

  base-domain = var.base-domain
}
module "configure_grist_form_for_deployment" {
  source  = "gitlab.com/vigigloo/tf-modules/gitlabk8svariables"
  version = "0.1.0"

  repositories = [var.repositories[1]]
  scope        = var.gitlab_environment_scope

  cluster_user           = module.namespace.user
  cluster_token          = module.namespace.token
  cluster_namespace      = module.namespace.namespace
  cluster_host           = var.kubeconfig.host
  cluster_ca_certificate = base64decode(var.kubeconfig.cluster_ca_certificate)

  base-domain = var.grist_form_base_domain
}

resource "gitlab_project_variable" "db_host" {
  project           = var.zonage_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_DBHOST"
  value = module.postgresql.host
}
resource "gitlab_project_variable" "db_port" {
  project           = var.zonage_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_DBPORT"
  value = module.postgresql.port
}
resource "gitlab_project_variable" "db_name" {
  project           = var.zonage_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_DBNAME"
  value = module.postgresql.dbname
}
resource "gitlab_project_variable" "db_user" {
  project           = var.zonage_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_DBUSER"
  value = module.postgresql.user
}
resource "gitlab_project_variable" "db_pass" {
  project           = var.zonage_project_id
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_DBPASS"
  value = module.postgresql.password
}

resource "gitlab_project_variable" "zonage_helm_values" {
  project           = var.zonage_project_id
  environment_scope = var.gitlab_environment_scope
  variable_type     = "file"
  protected         = false

  key   = "HELM_UPGRADE_VALUES"
  value = <<-EOT
  resources:
    limits:
      cpu: 500m
      memory: 1024Mi
    requests:
      cpu: 10m
      memory: 60Mi

  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.class: haproxy

  podAnnotations:
    monitoring-org-id: ${var.monitoring_org_id}

  service:
    targetPort: 80

  probes:
    liveness:
      tcpSocket:
        port: 80
    readiness:
      tcpSocket:
        port: 80

  monitoring:
    exporter:
      enabled: true
    dashboard:
      deploy: true

  EOT
}

moved {
  from = gitlab_project_variable.backend_helm_values
  to   = gitlab_project_variable.zonage_helm_values
}

resource "gitlab_project_variable" "grist_form_grist_instances" {
  count             = var.gitlab_environment_scope == "production" ? 1 : 0
  project           = var.repositories[1]
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_NEXT_PUBLIC_GRIST_INSTANCES"
  value = var.grist_form_grist_url
}
resource "gitlab_project_variable" "grist_form_grist_db_path" {
  count             = var.gitlab_environment_scope == "production" ? 1 : 0
  project           = var.repositories[1]
  environment_scope = var.gitlab_environment_scope

  key   = "HELM_ENV_VAR_GRIST_DB_URL"
  value = "${var.grist_form_grist_url}${var.grist_form_db_path}"
}
resource "random_password" "grist_form_crypto_passphrase" {
  special = false
  length  = 40
}
resource "kubernetes_secret_v1" "grist_form_secret" {
  count = var.gitlab_environment_scope == "production" ? 1 : 0
  metadata {
    name      = "grist-form-secrets"
    namespace = module.namespace.namespace
  }
  data = {
    GRIST_API_TOKEN   = var.grist_form_grist_api_token
    CRYPTO_PASSPHRASE = random_password.grist_form_crypto_passphrase.result
  }
}

resource "gitlab_project_variable" "grist_form_helm_values" {
  count             = var.gitlab_environment_scope == "production" ? 1 : 0
  project           = var.repositories[1]
  environment_scope = var.gitlab_environment_scope
  variable_type     = "file"
  protected         = false

  key   = "HELM_UPGRADE_VALUES"
  value = <<-EOT
  resources:
    limits:
      memory: 1024Mi
    requests:
      cpu: 10m

  ingress:
    enabled: true
    annotations:
      kubernetes.io/ingress.class: haproxy

  podAnnotations:
    monitoring-org-id: ${var.monitoring_org_id}

  service:
    targetPort: 3000

  probes:
    liveness:
      tcpSocket:
        port: 3000
    readiness:
      tcpSocket:
        port: 3000

  envVars:
    GRIST_API_TOKEN:
      secretKeyRef:
        name: ${kubernetes_secret_v1.grist_form_secret[0].metadata[0].name}
        key: GRIST_API_TOKEN
    CRYPTO_PASSPHRASE:
      secretKeyRef:
        name: ${kubernetes_secret_v1.grist_form_secret[0].metadata[0].name}
        key: CRYPTO_PASSPHRASE
  EOT
}

moved {
  from = gitlab_project_variable.grist_forms_grist_db_path[0]
  to   = gitlab_project_variable.grist_form_grist_db_path[0]
}
moved {
  from = gitlab_project_variable.grist_forms_grist_url[0]
  to   = gitlab_project_variable.grist_form_grist_url[0]
}

moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.base-domain["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.base-domain["37787354"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-ca["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.k8s-ca["37787354"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-host["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.k8s-host["37787354"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-namespace["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.k8s-namespace["37787354"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-token["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.k8s-token["37787354"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-user["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.k8s-user["37787354"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.base-domain["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.base-domain["37787354"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-ca["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.k8s-ca["37787354"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-host["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.k8s-host["37787354"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-namespace["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.k8s-namespace["37787354"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-token["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.k8s-token["37787354"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-user["37787354"]
  to   = module.configure_zonage_for_deployment.gitlab_project_variable.k8s-user["37787354"]
}

moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.base-domain["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.base-domain["46093914"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-ca["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.k8s-ca["46093914"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-host["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.k8s-host["46093914"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-namespace["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.k8s-namespace["46093914"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-token["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.k8s-token["46093914"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-user["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.k8s-user["46093914"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.base-domain["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.base-domain["46093914"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-ca["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.k8s-ca["46093914"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-host["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.k8s-host["46093914"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-namespace["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.k8s-namespace["46093914"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-token["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.k8s-token["46093914"]
}
moved {
  from = module.configure-repository-for-deployment.gitlab_project_variable.k8s-user["46093914"]
  to   = module.configure_grist_form_for_deployment.gitlab_project_variable.k8s-user["46093914"]
}
