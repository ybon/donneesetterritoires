# tflint-ignore: terraform_unused_declarations
variable "scaleway_organization_id" {
  type = string
}

variable "dev_base-domain" {
  type = string
}

variable "prod_base-domain" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "project_name" {
  type = string
}

variable "scaleway_project_project_id" {
  type = string
}
variable "scaleway_project_access_key" {
  type = string
}
variable "scaleway_project_secret_key" {
  type      = string
  sensitive = true
}

variable "scaleway_cluster_development_project_id" {
  type = string
}
variable "scaleway_cluster_development_access_key" {
  type = string
}
variable "scaleway_cluster_development_secret_key" {
  type      = string
  sensitive = true
}
variable "scaleway_cluster_development_cluster_id" {
  type = string
}
variable "scaleway_cluster_production_project_id" {
  type = string
}
variable "scaleway_cluster_production_access_key" {
  type = string
}
variable "scaleway_cluster_production_secret_key" {
  type      = string
  sensitive = true
}
variable "scaleway_cluster_production_cluster_id" {
  type = string
}

variable "gitlab_token" {
  type = string
}

variable "grafana_development_url" {
  type = string
}
variable "grafana_development_api_key" {
  type = string
}
variable "grafana_development_org_id" {
  type = string
}
variable "grafana_development_loki_url" {
  type = string
}
variable "grafana_development_prometheus_url" {
  type = string
}

variable "grafana_production_url" {
  type = string
}
variable "grafana_production_api_key" {
  type = string
}
variable "grafana_production_org_id" {
  type = string
}
variable "grafana_production_loki_url" {
  type = string
}
variable "grafana_production_prometheus_url" {
  type = string
}

variable "grist_monitoring_org_id" {
  type    = string
  default = null
}

variable "production_tools_grist_oauth_domain" {
  type = string
}
variable "production_tools_grist_oauth_client_id" {
  type = string
}
variable "production_tools_grist_oauth_client_secret" {
  type      = string
  sensitive = true
}
variable "production_tools_grist_default_email" {
  type = string
}

variable "development_tools_grist_oauth_domain" {
  type = string
}
variable "development_tools_grist_oauth_client_id" {
  type = string
}
variable "development_tools_grist_oauth_client_secret" {
  type      = string
  sensitive = true
}
variable "development_tools_grist_default_email" {
  type = string
}

variable "tools_grist_beta_oauth_domain" {
  type = string
}
variable "tools_grist_beta_oauth_client_id" {
  type = string
}
variable "tools_grist_beta_oauth_client_secret" {
  type      = string
  sensitive = true
}
variable "tools_grist_beta_default_email" {
  type = string
}

variable "production_grist_form_db_path" {
  type      = string
  sensitive = true
}
variable "production_grist_form_grist_api_token" {
  type      = string
  sensitive = true
}
variable "production_grist_form_base_domain" {
  type = string
}

variable "development_tools_umap_sso_endpoint" {
  type = string
}
variable "development_tools_umap_sso_key" {
  type = string
}
variable "development_tools_umap_sso_secret" {
  type      = string
  sensitive = true
}
