# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version     = "3.12.0"
  constraints = "~> 3.12.0"
  hashes = [
    "h1:DXO6Kwy9QpnFzJQ6NdkjZu4PnDHSYHMbvDxLlfEl0ho=",
    "h1:KQaF1MqHKwuhtI1r6KDWJbY1+PI9iczDoRQ9CfjHK4c=",
    "h1:xd7K0MzsZmRnMTeZFpfSaBM59d70DlkAUv/szwFYTwk=",
    "h1:zZKpUsf1STvLLyH30a6ZcjQoARU8MJGbHsGCjYHrnOA=",
    "zh:1c3e89cf19118fc07d7b04257251fc9897e722c16e0a0df7b07fcd261f8c12e7",
    "zh:207f7ffaaf16f3d0db9eec847867871f4a9eb9bb9109d9ab9c9baffade1a4688",
    "zh:2360bdd3253a15fbbb6581759aa9499235de670d13f02272cbe408cb425dc625",
    "zh:2a9bec466163baeb70474fab7b43aababdf28b240f2f4917299dfe1a952fd983",
    "zh:3a6ea735b5324c17aa6776bff3b2c1c2aeb2b6c20b9c944075c8244367eb7e4c",
    "zh:3aa73c622187c06c417e1cd98cd83f9a2982c478e4f9f6cd76fbfaec3f6b36e8",
    "zh:51ace107c8ba3f2bb7f7d246db6a0428ef94aafceca38df5908167f336963ec8",
    "zh:53a35a827596178c2a12cf33d1f0d0b4cf38cd32d2cdfe2443bdd6ebb06561be",
    "zh:5bece68a724bffd2e66293429319601d81ac3186bf93337c797511672f80efd0",
    "zh:60f21e8737be59933a923c085dcb7674fcd44d8a5f2e4738edc70ae726666204",
    "zh:9fb277f13dd8f81dee7f93230e7d1593aca49c4788360c3f2d4e42b8f6bb1a8f",
    "zh:ac63a9b4a3a50a3164ee26f1e64cc6878fcdb414f50106abe3dbeb7532edc8cd",
    "zh:ed083de9fce2753e3dfeaa1356508ecb0f218fbace8a8ef7b56f3f324fff593b",
    "zh:ed966026c76a334b6bd6d621c06c5db4a6409468e4dd99d9ad099d3a9de22768",
  ]
}

provider "registry.terraform.io/grafana/grafana" {
  version = "1.27.0"
  hashes = [
    "h1:Q2YgNsrrI0YolJi6i1HzidV0lqfIrA36dkDIclfEDps=",
    "h1:ifKgRrZHspeXMSavSCsWbLrk6tC1NOyWN0iiGv2j6I4=",
    "h1:qTAnpOmOQT9+bd6OJXHC7UmiJxAA45ZMLuX6qBU89c4=",
    "h1:vlRBpNfYF4GFQmxAn4FaHsGJ9hgb0s3IGYLqUil7GG4=",
    "zh:01ef0ae20530a54cbb4bffc35e97733916e5ae2e8f7fa00aefa2e86e24206823",
    "zh:08a4ac8b690bab9a3b454c3d998917f4ed49fc225a21ff53ceb0488eb4b9d15d",
    "zh:0e08516cd6c2495bc83a4a8e0252bfa70e310aa400af0fe766bbe7ddd05a21cb",
    "zh:14856865f6e6695e6d7708d70844a2c031cfc9b091e7cf530a453b2f78c9a691",
    "zh:2b1c05fff5011ab83acdd292484857fe886cd113abbb7fc617bbb8f358517cc0",
    "zh:31bae1b1c635a94329470b30986d336f4b3819bf24aacd953d5b57debb83bd4d",
    "zh:352b6ea190711c8f3f107540c8943c8f6b9faf4fbc73a9c1721b15db4a103edb",
    "zh:7eda29d30d451b842c5b0b2cf15cb907e76e8bac4843e90830a62a68bbe877a5",
    "zh:bd640d7e8a126d810a34766816b4e17a07c634ffef14b468269c8191683fff27",
    "zh:ddfa43a7b31fb840f04420c82fe0313a44fa5099c3d1f61219e630d6c8440e2d",
    "zh:e50dccaf8cb9922ac25e2f87a85083d5c2cef5323eac4ce7d933012af7a25e88",
    "zh:e72903aeb4830b7b89efcf7336a61c736d9049c4156b6f17cec51663ed6e803d",
    "zh:f4161d62960ec9f9d84cb73437a9b9195831c467cdcc3381e431fa6e2cd92a14",
    "zh:f4699da872dfc9847eb3da49fd6ae4943e92602b617931bb07b91e646d90a279",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version     = "2.10.1"
  constraints = "~> 2.0"
  hashes = [
    "h1:ctDhNJU4tEcyoUgPzwKuJmbDIqUl25mCY+s/lVHP6Sg=",
    "zh:0717312baed39fb0a00576297241b69b419880cad8771bf72dec97ebdc96b200",
    "zh:0e0e287b4e8429a0700143c8159764502eba0b33b1d094bf0d4ef4d93c7802cb",
    "zh:4f74605377dab4065aaad35a2c5fa6186558c6e2e57b9058bdc8a62cf91857b9",
    "zh:505f4af4dedb7a4f8f45b4201900b8e16216bdc2a01cc84fe13cdbf937570e7e",
    "zh:83f37fe692513c0ce307d487248765383e00f9a84ed95f993ce0d3efdf4204d3",
    "zh:840e5a84e1b5744f0211f611a2c6890da58016a40aafd5971f12285164d4e29b",
    "zh:8c03d8dee292fa0367b0511cf3e95b706e034f78025f5dff0388116e1798bf47",
    "zh:937800d1860f6b3adbb20e65f11e5fcd940b21ce8bdb48198630426244691325",
    "zh:c1853aa5cbbdd1d46f4b169e84c3482103f0e8575a9bb044dbde908e27348c5d",
    "zh:c9b0f640590da20931c30818b0b0587aa517d5606cb6e8052e4e4bf38f97b54d",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
    "zh:fe8bd4dd09dc7ca218959eda1ced9115408c2cdc9b4a76964bfa455f3bcadfd3",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version     = "2.13.1"
  constraints = ">= 2.13.0, ~> 2.13.0, ~> 2.13"
  hashes = [
    "h1:cN3OwZvhtn/y3XfnGQ4hi+7oZp1gU2zVYhznRv2C7Qg=",
    "zh:061f6ecbbf9a3c6345b56c28ebc2966a05d8eb02f3ba56beedd66e4ea308e332",
    "zh:2119beeccb35bc5d1392b169f9fc748865261b45fb75fc8f57200e91658837c6",
    "zh:26c29083d0d84fbc2e356e3dd1db3e2dc4139e943acf7a318d3c98f954ac6bd6",
    "zh:2fb5823345ab05b3df74bb5c51c61072637d01b3cddffe3ad36a73b7d5b749e6",
    "zh:3475b4422fffaf58584c4d877f98bfeff075e4a746f13e985d2cb20adc873a6c",
    "zh:366b4bef49932d1d71b12849c1878c254a887962ff915f37982299c1185dd48a",
    "zh:589f9358e4a4bd74a83b97ccc64df455ddfa64c4c4e099aef30fa29080497a8a",
    "zh:7a0d75e0e4fee6cc5599ac9d5e91de563ce9ea7bd8137480c7abd09642a9e72c",
    "zh:a297a42aefe0650e3d9fbe55a3ee48b14bb8bb5edb7068c09512d72afc3d9ca5",
    "zh:b7f83a89b646542d02b733d464e45d6d0739a9dbb921305e7b8347e9fc98a149",
    "zh:d4c721174a598b66bd1b29c40fa7cffafe90bb58186cd7506d792a6b04161103",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}

provider "registry.terraform.io/hashicorp/null" {
  version = "3.2.1"
  hashes = [
    "h1:FbGfc+muBsC17Ohy5g806iuI1hQc4SIexpYCrQHQd8w=",
    "zh:58ed64389620cc7b82f01332e27723856422820cfd302e304b5f6c3436fb9840",
    "zh:62a5cc82c3b2ddef7ef3a6f2fedb7b9b3deff4ab7b414938b08e51d6e8be87cb",
    "zh:63cff4de03af983175a7e37e52d4bd89d990be256b16b5c7f919aff5ad485aa5",
    "zh:74cb22c6700e48486b7cabefa10b33b801dfcab56f1a6ac9b6624531f3d36ea3",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:79e553aff77f1cfa9012a2218b8238dd672ea5e1b2924775ac9ac24d2a75c238",
    "zh:a1e06ddda0b5ac48f7e7c7d59e1ab5a4073bbcf876c73c0299e4610ed53859dc",
    "zh:c37a97090f1a82222925d45d84483b2aa702ef7ab66532af6cbcfb567818b970",
    "zh:e4453fbebf90c53ca3323a92e7ca0f9961427d2f0ce0d2b65523cc04d5d999c2",
    "zh:e80a746921946d8b6761e77305b752ad188da60688cfd2059322875d363be5f5",
    "zh:fbdb892d9822ed0e4cb60f2fedbdbb556e4da0d88d3b942ae963ed6ff091e48f",
    "zh:fca01a623d90d0cad0843102f9b8b9fe0d3ff8244593bd817f126582b52dd694",
  ]
}

provider "registry.terraform.io/hashicorp/random" {
  version     = "3.5.1"
  constraints = "~> 3.5.1"
  hashes = [
    "h1:VSnd9ZIPyfKHOObuQCaKfnjIHRtR7qTw19Rz8tJxm+k=",
    "zh:04e3fbd610cb52c1017d282531364b9c53ef72b6bc533acb2a90671957324a64",
    "zh:119197103301ebaf7efb91df8f0b6e0dd31e6ff943d231af35ee1831c599188d",
    "zh:4d2b219d09abf3b1bb4df93d399ed156cadd61f44ad3baf5cf2954df2fba0831",
    "zh:6130bdde527587bbe2dcaa7150363e96dbc5250ea20154176d82bc69df5d4ce3",
    "zh:6cc326cd4000f724d3086ee05587e7710f032f94fc9af35e96a386a1c6f2214f",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:b6d88e1d28cf2dfa24e9fdcc3efc77adcdc1c3c3b5c7ce503a423efbdd6de57b",
    "zh:ba74c592622ecbcef9dc2a4d81ed321c4e44cddf7da799faa324da9bf52a22b2",
    "zh:c7c5cde98fe4ef1143bd1b3ec5dc04baf0d4cc3ca2c5c7d40d17c0e9b2076865",
    "zh:dac4bad52c940cd0dfc27893507c1e92393846b024c5a9db159a93c534a3da03",
    "zh:de8febe2a2acd9ac454b844a4106ed295ae9520ef54dc8ed2faf29f12716b602",
    "zh:eab0d0495e7e711cca367f7d4df6e322e6c562fc52151ec931176115b83ed014",
  ]
}

provider "registry.terraform.io/hashicorp/time" {
  version     = "0.9.1"
  constraints = ">= 0.7.0"
  hashes = [
    "h1:NUv/YtEytDQncBQ2mTxnUZEy/rmDlPYmE9h2iokR0vk=",
    "zh:00a1476ecf18c735cc08e27bfa835c33f8ac8fa6fa746b01cd3bcbad8ca84f7f",
    "zh:3007f8fc4a4f8614c43e8ef1d4b0c773a5de1dcac50e701d8abc9fdc8fcb6bf5",
    "zh:5f79d0730fdec8cb148b277de3f00485eff3e9cf1ff47fb715b1c969e5bbd9d4",
    "zh:78d5eefdd9e494defcb3c68d282b8f96630502cac21d1ea161f53cfe9bb483b3",
    "zh:8c8094689a2bed4bb597d24a418bbbf846e15507f08be447d0a5acea67c2265a",
    "zh:a6d9206e95d5681229429b406bc7a9ba4b2d9b67470bda7df88fa161508ace57",
    "zh:aa299ec058f23ebe68976c7581017de50da6204883950de228ed9246f309e7f1",
    "zh:b129f00f45fba1991db0aa954a6ba48d90f64a738629119bfb8e9a844b66e80b",
    "zh:ef6cecf5f50cda971c1b215847938ced4cb4a30a18095509c068643b14030b00",
    "zh:f1f46a4f6c65886d2dd27b66d92632232adc64f92145bf8403fe64d5ffa5caea",
    "zh:f79d6155cda7d559c60d74883a24879a01c4d5f6fd7e8d1e3250f3cd215fb904",
    "zh:fd59fa73074805c3575f08cd627eef7acda14ab6dac2c135a66e7a38d262201c",
  ]
}

provider "registry.terraform.io/scaleway/scaleway" {
  version     = "2.2.9"
  constraints = "~> 2.2.0"
  hashes = [
    "h1:yRjYMvMcU9vvHg0zMWhebs6/JOhfQk2tJR2+njthRZI=",
    "zh:0c4474b3a9ab58db565e9df684364088ce9a46de0c6fa3cb610276a6e4bee07d",
    "zh:0de61684c1599e0595c3f252771efbb05c3e3fe12fb67f7e8906a5554cef12d1",
    "zh:13718f2b75d3ec3e4eae19f576bb01676242c23c14d2e9f7707ee6ab7f2bc636",
    "zh:1cc3febe7780d3b354de910991591f1ac20a1fe528fcbc23703c7e406a6e7c17",
    "zh:2fc2f95aeeec73e4dfc5c703f91a6aee7264d68edde99d5fd6f8881ccc44ff8f",
    "zh:38dbd61499e653092b08e353abda6d2fe0362685cff96ab4127ec556c6c694b6",
    "zh:3a57afe1f8142ed8995808dcee2f1f9373267a2fc58ade7730962cd338057b89",
    "zh:5410ae3e5ffbdd480d39af1fa6154783c8b13026cd8ff90d39b54f1aafaf3dcb",
    "zh:95a4e2278f49a35ac5b345d2ffdcf7f624fe3b5922b03c23ab1e8432690708cf",
    "zh:ba7acf4715085135c384d93f0ca74933e02d1beda68ce4c3cf2cbb3241cf3625",
    "zh:cfa4334f747651c7641790b7e2da913a38cfb7f68122b37c684c421a65be8528",
    "zh:d99969b25c6e65658e78bc26e48debd9e4cb66b5419ec551b41bd8ada92acf4b",
    "zh:defbe054accb2abaeb8a8b9116e7fab63b82b91282a89fa2cf279469b0910f2a",
    "zh:f0e76c46a6d34a7f6b471880445c7f48c52ccd9a3cf83563cb52d5bdde3cd3dc",
  ]
}
