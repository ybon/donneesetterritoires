terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.12.0"
    }
    scaleway = {
      source  = "scaleway/scaleway"
      version = "~> 2.2.0"
    }
    grafana = {
      source  = "grafana/grafana"
      version = "~> 1.27.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.5.1"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.13.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.0"
    }
  }
  required_version = ">= 0.14"
}
