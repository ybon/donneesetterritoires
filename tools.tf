module "gitlab-runner" {
  source  = "gitlab.com/vigigloo/tf-modules/k8sgitlabrunner"
  version = "0.2.3"

  chart_version = "0.47.1"

  kubeconfig    = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  project_slug  = var.project_slug
  project_name  = var.project_name
  gitlab_groups = [data.gitlab_group.donneesetterritoires.group_id]
}

module "addok" {
  source = "./tools/addok"

  kubeconfig   = data.scaleway_k8s_cluster.dev.kubeconfig[0]
  hostname     = "addok.${var.dev_base-domain}"
  project_slug = var.project_slug
}

locals {
  grist_beta_project_slug = "donnees-grist-beta"
}
module "grist_beta" {
  source              = "./tools/grist"
  default_email       = var.tools_grist_beta_default_email
  monitoring_org_id   = random_string.production_secret_org_id.result
  oauth_client_id     = var.tools_grist_beta_oauth_client_id
  oauth_client_secret = var.tools_grist_beta_oauth_client_secret
  oauth_domain        = var.tools_grist_beta_oauth_domain
  domain              = "grist.incubateur.net"
  project_slug        = local.grist_beta_project_slug
  scaleway_access_key = var.scaleway_project_access_key
  scaleway_secret_key = var.scaleway_project_secret_key
  backup_bucket       = "grist-beta-backups"

  # explicitly passing providers to avoid confusion
  providers = {
    helm       = helm.helm_production
    kubernetes = kubernetes.kubernetes_production
    scaleway   = scaleway.scaleway_project
  }
}

module "grist_dev" {
  source              = "./tools/grist_dev"
  scaleway_access_key = var.scaleway_cluster_development_access_key
  scaleway_secret_key = var.scaleway_cluster_development_secret_key
  scaleway_project_id = var.scaleway_cluster_development_project_id
  dns_zone_incubateur = "incubateur.anct.gouv.fr"
  oauth_domain        = var.development_tools_grist_oauth_domain
  oauth_client_id     = var.development_tools_grist_oauth_client_id
  oauth_client_secret = var.development_tools_grist_oauth_client_secret
  default_email       = var.development_tools_grist_default_email
  kubeconfig          = local.kubeconfig_development

  monitoring_org_id = var.grist_monitoring_org_id
  providers = {
    kubernetes = kubernetes.kubernetes_development
    helm       = helm.helm_development
  }
}

module "grist_prod" {
  source              = "./tools/grist_prod"
  scaleway_access_key = var.scaleway_cluster_production_access_key
  scaleway_secret_key = var.scaleway_cluster_production_secret_key
  scaleway_project_id = var.scaleway_cluster_production_project_id
  dns_zone_incubateur = "incubateur.anct.gouv.fr"
  oauth_client_id     = var.production_tools_grist_oauth_client_id
  oauth_client_secret = var.production_tools_grist_oauth_client_secret
  oauth_domain        = var.production_tools_grist_oauth_domain
  default_email       = var.production_tools_grist_default_email

  monitoring_org_id = var.grist_monitoring_org_id
  providers = {
    helm       = helm.helm_production
    kubernetes = kubernetes.kubernetes_production
  }
}

resource "scaleway_object_bucket" "umap_dev_backups" {
  provider = scaleway.scaleway_project
  name     = "donnees-umap-dev-backups"
}
module "umap_dev" {
  source                   = "./tools/umap"
  domain                   = "umap.dev.incubateur.anct.gouv.fr"
  monitoring_org_id        = random_string.development_secret_org_id.result
  sso_endpoint             = var.development_tools_umap_sso_endpoint
  sso_key                  = var.development_tools_umap_sso_key
  sso_secret               = var.development_tools_umap_sso_secret
  namespace                = "donnees-umap"
  image_repository         = "registry.gitlab.com/incubateur-territoires/startups/donnees-et-territoires/umap-dsfr-moncomptepro/main"
  image_tag                = "7f2816eec4bd0da87ab6fe599100f395b8c8fd53"
  backup_bucket_name       = scaleway_object_bucket.umap_dev_backups.name
  backup_bucket_access_key = var.scaleway_project_access_key
  backup_bucket_secret_key = var.scaleway_project_secret_key
  backup_bucket_endpoint   = "s3.fr-par.scw.cloud"
  backup_bucket_region     = "fr-par"

  providers = {
    kubernetes = kubernetes.kubernetes_development
    helm       = helm.helm_development
  }
}
