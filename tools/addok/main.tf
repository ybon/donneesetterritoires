module "namespace" {
  source  = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version = "0.3.3"

  namespace    = "${var.project_slug}-addok"
  max_cpu      = 4
  max_memory   = "17Gi"
  project_name = "Addok"
  project_slug = "addok"
}

resource "helm_release" "addok" {
  namespace  = module.namespace.namespace
  name       = "addok"
  chart      = "addok"
  repository = "https://gitlab.com/api/v4/projects/41925207/packages/helm/stable"
  version    = "0.1.0"

  values = [
    <<-EOT
    ingress:
      enabled: true
      host: ${var.hostname}
      annotations:
        acme.cert-manager.io/http01-edit-in-place: "true"
        cert-manager.io/cluster-issuer: letsencrypt-prod
        kubernetes.io/ingress.class: haproxy
    resources:
      limits:
        cpu: 1
        memory: 128Mi
      requests:
        cpu: 100m
        memory: 128Mi
    redis:
      resources:
        limits:
          cpu: 1
          memory: 8Gi
        requests:
          cpu: 100m
          memory: 6Gi
    affinity:
      podAffinity:
        requiredDuringSchedulingIgnoredDuringExecution:
        - labelSelector:
            matchExpressions:
            - key: app.kubernetes.io/name
              operator: In
              values:
              - addok
          topologyKey: kubernetes.io/hostname
    EOT
    , <<-EOT
    configuration: |
      ${indent(2, file("${path.module}/config.py"))}
    EOT
  ]
}
