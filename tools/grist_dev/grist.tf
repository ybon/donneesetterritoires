locals {
  domain             = "grist.dev.${var.dns_zone_incubateur}"
  grist_release_name = "grist"
}

resource "scaleway_object_bucket" "grist_backups" {
  name   = "anct-dev-grist-backups"
  region = "fr-par"
}

resource "scaleway_object_bucket" "grist_snapshots" {
  name   = "anct-dev-grist-snapshots"
  region = "fr-par"
  versioning {
    enabled = true
  }
}

resource "random_password" "redis_password" {
  length  = 32
  special = false
}

module "redis" {
  source        = "gitlab.com/vigigloo/tools-k8s/redis"
  version       = "0.1.1"
  namespace     = module.grist_namespace.namespace
  chart_name    = "redis"
  chart_version = "17.4.2"

  redis_password = resource.random_password.redis_password.result
  redis_replicas = 0
}

module "grist_namespace" {
  source            = "gitlab.com/vigigloo/tf-modules/k8slimitednamespace"
  version           = "1.0.1"
  max_cpu_requests  = "5"
  max_memory_limits = "12Gi"
  namespace         = "grist"
  project_name      = "Grist"
  project_slug      = "grist"

  default_container_cpu_requests  = "200m"
  default_container_memory_limits = "128Mi"
}

module "grist" {
  source     = "gitlab.com/vigigloo/tools-k8s/grist"
  version    = "1.1.1"
  namespace  = module.grist_namespace.namespace
  chart_name = local.grist_release_name
  values = [
    <<-EOT
    envVars:
      APP_HOME_URL: https://${local.domain}
      GRIST_FORWARD_AUTH_HEADER: X-Forwarded-User
      GRIST_FORWARD_AUTH_LOGOUT_PATH: _oauth/logout
      GRIST_ORG_IN_PATH: "true"
      GRIST_DEFAULT_EMAIL: "${var.default_email}"
      GRIST_SANDBOX_FLAVOR: gvisor
      GRIST_WIDGET_LIST_URL: https://infrastructure.pages.sit.incubateur.tech/grist-widgets/manifest.json
      GRIST_HIDE_UI_ELEMENTS: billing,sendToDrive
      GRIST_ALLOWED_HOSTS: datahub.incubateur.tech,localhost
      APP_STATIC_INCLUDE_CUSTOM_CSS: true
      GRIST_DEFAULT_LOCALE: fr
      GRIST_HELP_CENTER: https://outline.incubateur.anct.gouv.fr/doc/documentation-grist-YPWlYTHa8j
      GRIST_DOCS_MINIO_BUCKET: ${scaleway_object_bucket.grist_snapshots.name}
      GRIST_DOCS_MINIO_ENDPOINT: s3.fr-par.scw.cloud
      GRIST_DOCS_MINIO_BUCKET_REGION: fr-par
      EOT
    , <<-EOT
    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}
    envVars:
      GRIST_DOCS_MINIO_ACCESS_KEY: ${var.scaleway_access_key}
      GRIST_DOCS_MINIO_SECRET_KEY: ${var.scaleway_secret_key}
      REDIS_URL: redis://default:${resource.random_password.redis_password.result}@${module.redis.hostname}
    EOT
  ]
  limits_cpu      = 1
  limits_memory   = "8Gi"
  requests_cpu    = "10m"
  requests_memory = "4Gi"

  backup_limits_cpu      = 1
  backup_limits_memory   = "1Gi"
  backup_requests_cpu    = "100m"
  backup_requests_memory = "512Mi"

  grist_persistence_size     = "20Gi"
  grist_backup_schedule      = "0 3 * * 1"
  grist_backup_s3_endpoint   = scaleway_object_bucket.grist_backups.endpoint
  grist_backup_s3_region     = scaleway_object_bucket.grist_backups.region
  grist_backup_s3_bucket     = scaleway_object_bucket.grist_backups.name
  grist_backup_s3_access_key = var.scaleway_access_key
  grist_backup_s3_secret_key = var.scaleway_secret_key

  image_repository = "gristlabs/grist@sha256"
  image_tag        = "bce3112f3b0bb1b813810f383333888575f8cb37267a534ed7da24c18c9fb057"

  grist_mount_files = [
    {
      path    = "/grist/static/ui-icons/Logo/logo_anct_2022.svg"
      content = filebase64("${path.module}/logo_anct_2022.svg")
    },
    {
      path    = "/grist/static/custom.css"
      content = filebase64("${path.module}/custom.css")
    }
  ]
}

resource "random_password" "auth_secret" {
  length  = 64
  special = false
}

module "auth" {
  source     = "gitlab.com/vigigloo/tools-k8s/traefikforwardauth"
  version    = "0.1.1"
  namespace  = module.grist_namespace.namespace
  chart_name = "auth"
  values = [
    <<-EOT
    ingress:
      annotations:
        haproxy.org/cors-enable: "true"
        haproxy.org/cors-allow-origin: "^https://.*(.anct.gouv.fr|incubateur.tech)$"
        haproxy.org/cors-allow-methods: "GET"
        haproxy.org/cors-allow-headers: "*"
    middleware:
      envVars:
        INSECURE_COOKIE: "true"
        DEFAULT_PROVIDER: "generic-oauth"
        PROVIDERS_GENERIC_OAUTH_AUTH_URL: "https://${var.oauth_domain}/oauth/authorize"
        PROVIDERS_GENERIC_OAUTH_TOKEN_URL: "https://${var.oauth_domain}/oauth/token"
        PROVIDERS_GENERIC_OAUTH_USER_URL: "https://${var.oauth_domain}/oauth/userinfo"
        PROVIDERS_GENERIC_OAUTH_CLIENT_ID: "${var.oauth_client_id}"
        PROVIDERS_GENERIC_OAUTH_CLIENT_SECRET: "${var.oauth_client_secret}"
        PROVIDERS_GENERIC_OAUTH_SCOPE: "openid email organizations"
        SECRET: ${random_password.auth_secret.result}
        LOGOUT_REDIRECT: "https://${local.domain}/signed-out"
EOT
  ]
  limits_cpu      = 1
  limits_memory   = "1Gi"
  requests_cpu    = "10m"
  requests_memory = "40Mi"
  ingress_host    = local.domain
  auth_paths = [
    "/auth/login",
    "/_oauth"
  ]
  auth_target_service = "http://${local.grist_release_name}"
}
