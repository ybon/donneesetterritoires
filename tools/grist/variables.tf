variable "domain" {
  type = string
}

variable "project_slug" {
  type = string
}

variable "oauth_domain" {
  type = string
}

variable "oauth_client_id" {
  type = string
}

variable "oauth_client_secret" {
  type      = string
  sensitive = true
}

variable "default_email" {
  type = string
}

variable "monitoring_org_id" {
  type      = string
  sensitive = true
}

variable "backup_bucket" {
  type = string
}

variable "scaleway_access_key" {
  type      = string
  sensitive = true
}
variable "scaleway_secret_key" {
  type      = string
  sensitive = true
}
