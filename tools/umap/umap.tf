resource "random_password" "umap_secret_key" {
  length  = 64
  special = false
}
resource "helm_release" "umap" {
  namespace  = module.namespace.namespace
  name       = "umap"
  chart      = "webapp"
  repository = "https://gitlab.com/api/v4/projects/47579096/packages/helm/stable"
  version    = "0.1.0"

  values = [
    <<-EOT
    image:
      repository: ${var.image_repository}
      tag: ${var.image_tag}
    ingress:
      enabled: true
      annotations:
        kubernetes.io/ingress.class: haproxy
        acme.cert-manager.io/http01-edit-in-place: "true"
      host: ${var.domain}
      hosts:
        - host: ${var.domain}
          paths:
            - path: /
              pathType: Prefix
      tls:
        enabled: true
    envVars:
      DATABASE_URL: "postgis://${urlencode(module.postgresql.user)}:${urlencode(module.postgresql.password)}@${module.postgresql.host}/${module.postgresql.dbname}"
      SITE_URL: "https://${var.domain}"
      UMAP_ALLOW_ANONYMOUS: False
      UMAP_USE_UNACCENT: True
      SECRET_KEY: ${random_password.umap_secret_key.result}
      ALLOWED_HOSTS: "*"
      SOCIAL_AUTH_MONCOMPTEPRO_OIDC_ENDPOINT: ${var.sso_endpoint}
      SOCIAL_AUTH_MONCOMPTEPRO_KEY: ${var.sso_key}
      SOCIAL_AUTH_MONCOMPTEPRO_SECRET: ${var.sso_secret}
    podAnnotations:
      monitoring-org-id: ${var.monitoring_org_id}
    resources:
      requests:
        cpu: 100m
      limits:
        memory: 1Gi
    service:
      targetPort: 8000
    persistence:
      uploads:
        size: 10Gi
        mountPath: /srv/umap/uploads
    EOT
  ]
}
