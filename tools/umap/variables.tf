variable "domain" {
  type = string
}

variable "monitoring_org_id" {
  type      = string
  sensitive = true
}

variable "sso_endpoint" {
  type = string
}
variable "sso_key" {
  type = string
}
variable "sso_secret" {
  type      = string
  sensitive = true
}

variable "namespace" {
  type = string
}
variable "namespace_quota_max_cpu" {
  type    = number
  default = 2
}
variable "namespace_quota_max_memory" {
  type    = string
  default = "12Gi"
}

variable "project_slug" {
  type    = string
  default = "umap"
}
variable "project_name" {
  type    = string
  default = "umap"
}

variable "image_repository" {
  type = string
}
variable "image_tag" {
  type = string
}

variable "backup_bucket_name" {
  type = string
}
variable "backup_bucket_endpoint" {
  type = string
}
variable "backup_bucket_region" {
  type = string
}
variable "backup_bucket_access_key" {
  type = string
}
variable "backup_bucket_secret_key" {
  type      = string
  sensitive = true
}
