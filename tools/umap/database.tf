module "postgresql" {
  source  = "gitlab.com/vigigloo/tools-k8s-crunchydata/pgcluster"
  version = "0.0.22"

  chart_name = "postgresql"
  namespace  = module.namespace.namespace
  values = [
    file("${path.module}/database.yaml"),
  ]
  pg_replicas    = 2
  pg_volume_size = "5Gi"
}
locals {
  db_secret_name = "postgresql-pguser-postgresql"
}

resource "kubernetes_cron_job_v1" "database_backups" {
  metadata {
    name      = "umap-database-backups"
    namespace = module.namespace.namespace
  }

  spec {
    concurrency_policy            = "Replace"
    failed_jobs_history_limit     = 10
    schedule                      = "25 3 * * *"
    starting_deadline_seconds     = 10
    successful_jobs_history_limit = 10

    job_template {
      metadata {
        annotations = {}
        labels      = {}
      }

      spec {
        backoff_limit = 3

        template {
          metadata {
            annotations = {
              monitoring-org-id = var.monitoring_org_id
            }
            labels = {}
          }
          spec {
            container {
              name  = "backup"
              image = "registry.gitlab.com/incubateur-territoires/startups/donnees-et-territoires/umap-dsfr-moncomptepro/backup:855689e8ebbf501bbdc3e20f1ba5530d8d99f231"

              resources {
                limits = {
                  memory = "512Mi"
                }
                requests = {
                  cpu = "100m"
                }
              }

              env {
                name  = "S3_BUCKET_NAME"
                value = var.backup_bucket_name
              }
              env {
                name  = "S3_REGION"
                value = var.backup_bucket_region
              }
              env {
                name  = "S3_ENDPOINT_URL"
                value = var.backup_bucket_endpoint
              }
              env {
                name  = "S3_ACCESS_KEY"
                value = var.backup_bucket_access_key
              }
              env {
                name  = "S3_SECRET_KEY"
                value = var.backup_bucket_secret_key
              }

              env {
                name = "POSTGRESQL_HOST"
                value_from {
                  secret_key_ref {
                    name = local.db_secret_name
                    key  = "host"
                  }
                }
              }
              env {
                name = "POSTGRESQL_PORT"
                value_from {
                  secret_key_ref {
                    name = local.db_secret_name
                    key  = "port"
                  }
                }
              }
              env {
                name = "POSTGRESQL_DATABASE"
                value_from {
                  secret_key_ref {
                    name = local.db_secret_name
                    key  = "dbname"
                  }
                }
              }
              env {
                name = "POSTGRESQL_USER"
                value_from {
                  secret_key_ref {
                    name = local.db_secret_name
                    key  = "user"
                  }
                }
              }
              env {
                name = "POSTGRESQL_PASSWORD"
                value_from {
                  secret_key_ref {
                    name = local.db_secret_name
                    key  = "password"
                  }
                }
              }

              volume_mount {
                name       = "data"
                mount_path = "/data"
                read_only  = true
              }
            }

            affinity {
              pod_affinity {
                required_during_scheduling_ignored_during_execution {
                  label_selector {
                    match_expressions {
                      key      = "app.kubernetes.io/instance"
                      operator = "In"
                      values   = ["umap"]
                    }
                    match_expressions {
                      key      = "app.kubernetes.io/name"
                      operator = "In"
                      values   = ["webapp"]
                    }
                  }
                  topology_key = "kubernetes.io/hostname"
                }
              }
            }

            volume {
              name = "data"
              persistent_volume_claim {
                claim_name = "umap-webapp-uploads"
              }
            }
          }
        }
      }
    }
  }
}
