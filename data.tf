data "scaleway_k8s_cluster" "prod" {
  provider   = scaleway.scaleway_production
  cluster_id = var.scaleway_cluster_production_cluster_id
}

data "scaleway_k8s_cluster" "dev" {
  provider   = scaleway.scaleway_development
  cluster_id = var.scaleway_cluster_development_cluster_id
}

data "gitlab_project" "zonage" {
  id = 37787354
}

data "gitlab_project" "grist_form" {
  id = 46093914
}

data "gitlab_group" "donneesetterritoires" {
  group_id = 55524135
}
